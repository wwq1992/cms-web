import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/iconfont/iconfont.css'
import './registerServiceWorker'
import 'normalize.css/normalize.css'

import { 
  Pagination,
  Button, 
  Card,
  Dropdown,
  DropdownMenu,
  DropdownItem,
	Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  Form,
  FormItem,
  Tabs,
  TabPane,
  Table,
  TableColumn,
  Message,
  Input } from 'element-ui';

Vue.use(Pagination)
Vue.use(Button)
Vue.use(Card)
Vue.use(Dropdown)
Vue.use(DropdownMenu)
Vue.use(DropdownItem)
Vue.use(Menu)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Submenu)
Vue.use(MenuItem)
Vue.use(MenuItemGroup)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Input)



Vue.config.productionTip = false

//注册vue-router的全局守卫
router.beforeEach((to, from, next) => {
	if( !sessionStorage.hasGo && to.name != 'login'){	//如果没有登录且前往的页面不是登录页
		next('/');
	}else if(sessionStorage.hasGo && to.name === 'login'){
		next('/menu');
	}else{
		next();
	}
});


Vue.prototype.$message = Message;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
