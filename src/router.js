import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/menu',
      name: '首页',
      component: () => import('./views/menu.vue'),
      children:[
        {
          path: '/',
          name: '网站列表',
          component: () => import('./components/list/websiteList.vue')
        },
        {
          path: '/webpageList/:id',
          name: '网页列表',
          component: () => import('./components/list/webpageList')
        },
        {
          path: '/comList',
          name: '组件列表',
          component: () =>  import('./components/list/comList.vue')
        },
        {
          path: '/user/personal',
          name: '个人中心',
          component: () => import('./components/user/personal.vue')
        },
        {
          path: '/user/changePassword',
          name: '修改密码',
          component: () => import('./components/user/changePassword.vue')
        }
      ]
    },
    {
      path: '/about',
      name: '关于我们',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/login.vue')
    }
  ]
})
