import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        tabsValue:JSON.parse(localStorage.getItem('tabsValue')) || '/',
        tabsList:JSON.parse(localStorage.getItem('tabsList')) || [],
    },
    mutations:{
        addTab(state,data){
            let tabIndex = -1
            tabIndex = state.tabsList.findIndex((item)=>{
                return item.path.toLowerCase() == data.path.toLowerCase();
            })        
            if(tabIndex < 0){
                //加入并激活
                state.tabsList.push(data)
                state.tabsValue = data.path

                localStorage.setItem('tabsValue', JSON.stringify(state.tabsValue));
                localStorage.setItem('tabsList', JSON.stringify(state.tabsList));//存起来
            }else{
                //直接激活
                state.tabsValue = data.path  
                localStorage.setItem('tabsValue', JSON.stringify(state.tabsValue));           
            }
        },
        setTabsValue(state,data){
            state.tabsValue = data
        },
        setTabsList(state,data){
            state.tabsList = data
        }
    }
})